package bot;

import bot.settings.Settings;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.guild.member.GuildMemberRoleAddEvent;
import net.dv8tion.jda.api.exceptions.HierarchyException;
import net.dv8tion.jda.api.exceptions.InsufficientPermissionException;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;


/**
 * This listener is mainly to make Sheeple more compatible with Mee6 and our current server setup which relies on every other role being stripped of muted people
 */
public class NewMutedMemberListener extends ListenerAdapter {
    @Override
    public void onGuildMemberRoleAdd(@NotNull GuildMemberRoleAddEvent event) {
        var addedRoles = event.getRoles();
        Guild guild = event.getGuild();
        Role mutedRole = guild.getRoleById(Settings.getInstance().getMutedRoleID());
        Member member = event.getMember();

        if (mutedRole == null) {
            return;
        }

        if (addedRoles.contains(mutedRole)) {
            try {
                if (guild.getSelfMember().canInteract(member)) {
                    guild.modifyMemberRoles(member, mutedRole).queue();
                }
            }
            catch(InsufficientPermissionException | HierarchyException | IllegalArgumentException e) {
                return;
            }
        }
    }
}
