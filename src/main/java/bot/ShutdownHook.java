package bot;

import bot.commands.Command;
import bot.settings.Settings;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.exceptions.ErrorResponseException;
import sun.misc.Signal;
import sun.misc.SignalHandler;

import java.util.Collection;
import java.util.concurrent.RejectedExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ShutdownHook extends Thread implements SignalHandler {

    private int interruptionCounter = 0; //counts the number of interrupt signals
    private JDA api;
    private Collection<Command> commands;

    public ShutdownHook(JDA api, Collection<Command> commands) {
        this.api = api;
        this.commands = commands;
    }
    @Override
    public void handle(Signal signal) {
        synchronized (this) {
            interruptionCounter++;
        }
        if (interruptionCounter == 1) {
            Logger.getLogger(Settings.LOGGER_NAME).log(Level.INFO, "Unregistering commands...");
            try {
                for (Guild g : api.getGuilds()) {
                    synchronized (Settings.getInstance().getGlobalLock()) {
                        g.updateCommands().complete();
                        Logger.getLogger(Settings.LOGGER_NAME).log(Level.INFO,
                                () -> String.format("Unregistered %d commands from %s",
                                        Settings.getInstance().getCommands().size(), g.getName()));
                    }
                }
            } catch(RejectedExecutionException e) {
                Logger.getLogger(Settings.LOGGER_NAME).log(Level.WARNING, "Event expired");
            } catch(ErrorResponseException e) {
                Logger.getLogger(Settings.LOGGER_NAME).log(Level.SEVERE, e.getMessage());
            }
        }
        System.exit(0);
    }

    @Override
    public void run() {
        this.handle(new Signal("INT"));
    }
}
