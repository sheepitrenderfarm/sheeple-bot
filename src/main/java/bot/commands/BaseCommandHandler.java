package bot.commands;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;

public abstract class BaseCommandHandler implements CommandHandler {

    protected CommandData commandData;

    public RequirementsCheckResult meetsRequirements(Command command, SlashCommandInteractionEvent event) {
        return event.getMember().hasPermission(command.getRequiredPermission()) ? RequirementsCheckResult.OK : RequirementsCheckResult.NO_PERMISSION;
    }

    @Override
    public void registerCommand(Command command, JDA api) {
        api.upsertCommand(command.getName(), command.getDescription()).queue();
    }

    @Override
    public void registerCommand(Command command, Guild guild) {
        guild.upsertCommand(command.getName(), command.getDescription()).queue();
    }
}
