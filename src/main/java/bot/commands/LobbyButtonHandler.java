package bot.commands;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.components.buttons.Button;
import net.dv8tion.jda.api.utils.messages.MessageCreateBuilder;

import java.util.Optional;

public class LobbyButtonHandler extends BaseCommandHandler {

    public static final String BUTTON_ID = "registerButton";
    private static final String WELCOME_MSG_OPTION = "welcome_msg_option";

    public LobbyButtonHandler() {

    }

    @Override
    public Optional<MessageEmbed> handle(Command command, SlashCommandInteractionEvent event) {
        var msgOption = event.getOption(WELCOME_MSG_OPTION);
        String msg = "I have read the rules";
        if (msgOption != null) {
            msg = msgOption.getAsString();
        }
        MessageCreateBuilder messageBuilder = new MessageCreateBuilder();
        messageBuilder.addActionRow(Button.secondary(BUTTON_ID, "Confirm"));
        messageBuilder.setContent(msg);
        event.reply(messageBuilder.build()).queue();
        return Optional.empty();
    }

    @Override
    public void registerCommand(Command command, Guild guild) {
        guild.upsertCommand(command.getName(), command.getDescription())
                .addOption(OptionType.STRING, WELCOME_MSG_OPTION, "What message should be shown above the button", false)
                .queue();
    }

    @Override
    public void registerCommand(Command command, JDA api) {
        api.upsertCommand(command.getName(), command.getDescription())
                .addOption(OptionType.STRING, WELCOME_MSG_OPTION, "What message should be shown above the button", false)
                .queue();
    }


}
