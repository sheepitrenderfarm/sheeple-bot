package bot.commands;

import bot.StatisticsCollector;
import bot.settings.Settings;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.utils.FileUpload;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServerStatsHandler extends BaseCommandHandler {
    private static final String FILE_NAME = StatisticsCollector.DIAGRAM_IMAGE_FILE_NAME;

    @Override
    public Optional<MessageEmbed> handle(Command command, SlashCommandInteractionEvent event) {

        if (!event.isAcknowledged()) {
            event.deferReply().queue();
        }
        Logger log = Logger.getLogger(Settings.LOGGER_NAME);
        String threw = null;

        try {
            Path path = Paths.get(FILE_NAME);
            File file = null;
            if (Files.notExists(path)) {
                log.log(Level.INFO, () -> String.format("File %s not found. Creating...", FILE_NAME));

                file = Settings.getInstance().getStatistics().saveGraph();
                if (file == null) {
                    log.log(Level.WARNING, () -> "Failed to create statistics file!");
                    return Optional.of(Settings.getInstance().createMessage("Failed to create statistics file!"));
                }
            }
            else {
                file = path.toFile();
            }
            MessageEmbed embed = Settings.getInstance().createFileMessage("Joined Member Statistics", file);

            if (event.isAcknowledged()) {
                event.getHook().editOriginalEmbeds(embed).setFiles(FileUpload.fromData(file)).queue();
            }
            else {
                event.replyEmbeds(embed).addFiles(FileUpload.fromData(file, FILE_NAME)).queue();
            }
            return Optional.empty();
        }
        catch (Exception e) {
            threw = e.getMessage();
        }
        return Optional.of(Settings.getInstance().createMessage(threw));
    }
}
