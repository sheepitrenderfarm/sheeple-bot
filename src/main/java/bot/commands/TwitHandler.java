package bot.commands;

import bot.settings.Settings;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.exceptions.ErrorResponseException;
import net.dv8tion.jda.api.exceptions.HierarchyException;
import net.dv8tion.jda.api.exceptions.InsufficientPermissionException;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

import java.time.Duration;
import java.time.LocalTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Puts guild members in a timeout. The bot will ignore them for the amount of time specified.
 */
public class TwitHandler extends BaseCommandHandler {
    private static final Duration DEFAULT_IGNORE_TIME = Duration.ofMinutes(30);
    public static final OptionData TWIT_OPTION = new OptionData(OptionType.USER,"twit", "The twit that shall be ignored", true);
    public static final OptionData IGNORE_DURATION_OPTION = new OptionData(OptionType.INTEGER,"ignore_duration_minutes", "How long the member shall be ignored. Default: " + DEFAULT_IGNORE_TIME.toMinutes() + " min");

    @Override
    public Optional<MessageEmbed> handle(Command command, SlashCommandInteractionEvent event) {
        var twitOption = event.getOption(TWIT_OPTION.getName());
        if (twitOption == null) {
            return Optional.of(Settings.getInstance().createMessage("You forgot to mention (a) twit(s)"));
        }

        User twit =twitOption.getAsUser();

        var ignoreDuration = event.getOption(IGNORE_DURATION_OPTION.getName(), Settings.TWIT_IGNORE_TIME_MINUTES,
                optionMapping -> optionMapping.getAsLong() < 1 ?
                        Settings.TWIT_IGNORE_TIME_MINUTES :
                        Duration.ofMinutes(optionMapping.getAsLong()));


        try {
            LocalTime time = LocalTime.now().plus(ignoreDuration);
            String twitID = twit.getId();
            Settings.getInstance().getIgnoredMembers().remove(twitID);
            Settings.getInstance().getIgnoredMembers().put(twitID, time);

        } catch (InsufficientPermissionException | HierarchyException e) {
            Logger.getLogger(Settings.LOGGER_NAME).log(Level.SEVERE, "Exception: ", e);
            return Optional.of(Settings.getInstance().createMessage(e.getMessage()));
        } catch (ErrorResponseException e) {
            Logger.getLogger(Settings.LOGGER_NAME).log(Level.SEVERE, "Exception: ", e);
            return Optional.empty();
        }

        String output = String.format(command.getOutput(), twit.getAsTag(), Settings.TWIT_IGNORE_TIME_MINUTES.toMinutes());
        MessageEmbed msg = Settings.getInstance().createMessage(output);
        return Optional.of(msg);
    }

    @Override
    public RequirementsCheckResult meetsRequirements(Command command, SlashCommandInteractionEvent event) {
        Member caller = event.getMember();
        Guild guild = event.getGuild();
        User twit = event.getOption(TWIT_OPTION.getName()).getAsUser();
        List<Role> sheepItRoles = Settings.getInstance().getSuperRoleIDs().stream()
                .map(r -> event.getGuild().getRoleById(r)).collect(Collectors.toList());

        boolean callerHasSheepItRole = !Collections.disjoint(caller.getRoles(), sheepItRoles);
        boolean twitIsInvalid = false;
        if(twit == null) {  //twit is not a member of this guild
            twitIsInvalid = false;
        }
        else {
            Member twitMember = guild.getMember(twit);
            if (twitMember == null) {
                twitIsInvalid = false;  //allow non-server members to be ignored i.e. when they left on bad terms
            }
            else {
                twitIsInvalid = !Collections.disjoint(twitMember.getRoles(), sheepItRoles) || twitMember.hasPermission(Permission.MANAGE_ROLES);
            }
        }

        if(twitIsInvalid) {
            return RequirementsCheckResult.INVALID_ARGUMENT;
        }
        if(super.meetsRequirements(command, event).equals(RequirementsCheckResult.OK) || callerHasSheepItRole) {
            return RequirementsCheckResult.OK;
        }
        else {
            return RequirementsCheckResult.MUST_BE_SHEEPMIN;
        }
    }

    @Override
    public void registerCommand(Command command, JDA api) {
        api.upsertCommand(command.getName(), command.getDescription())
                .addOptions(
                        TWIT_OPTION,
                        IGNORE_DURATION_OPTION
                )
                .queue();
    }

    @Override
    public void registerCommand(Command command, Guild guild) {
        guild.upsertCommand(command.getName(), command.getDescription())
                .addOptions(
                        TWIT_OPTION,
                        IGNORE_DURATION_OPTION
                )
                .queue();
    }
}
