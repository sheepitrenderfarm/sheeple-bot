package bot.errorhandlers;

import bot.settings.Settings;

import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FailedInteractionErrorHandler implements Consumer<Throwable> {
    @Override
    public void accept(Throwable throwable) {
        Logger.getLogger(Settings.LOGGER_NAME).log(Level.WARNING, throwable::getMessage);
    }
}
