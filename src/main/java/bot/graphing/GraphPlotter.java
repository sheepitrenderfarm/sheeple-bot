package bot.graphing;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

public class GraphPlotter {
    private static final int HEIGHT = 500;
    private static final int WIDTH = 800;
    private static final String FONT = "fonts/VeranoSans-Light.ttf";

    public GraphPlotter() {

    }

    public BufferedImage plotGraphs(List<String> abscissaLabels, String diagramTitle, Graph... graphs) throws IOException, FontFormatException {
        return plotGraphs(WIDTH, HEIGHT, abscissaLabels, diagramTitle, graphs);
    }

    public BufferedImage plotGraphs(int imageWidth, int imageHeight, List<String> abscissaLabels, String diagramTitle, Graph... graphs) throws IOException, FontFormatException {

        //Find the maximum out of all lists provided
        var totalMaximum = Stream.of(graphs).flatMap(graph -> graph.getValues().stream())
                .filter(Objects::nonNull)
                .max(Comparator.naturalOrder()).orElse(Integer.MIN_VALUE);  //find  the graph with the highest maximum

        if (totalMaximum == Integer.MIN_VALUE) {
            System.err.println("Could not find maximum");
            return null;
        }

        int diagramMax = Math.round(totalMaximum + 5);
        assert diagramMax > totalMaximum;

        BufferedImage bufferedImage = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics2D = bufferedImage.createGraphics();
        graphics2D.setClip(new Rectangle(imageWidth, imageHeight));
        graphics2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        drawBackground(graphics2D, Color.WHITE);
        drawTitle(graphics2D, diagramTitle, new Color(99, 111, 197));
        graphics2D.setClip(getScaledClip(graphics2D.getClip(), 0.1f));
        drawBackground(graphics2D, Color.LIGHT_GRAY);
        drawRaster(graphics2D, Color.BLACK, 5, diagramMax, abscissaLabels.subList(0, graphs[0].getValues().size()));    //This assumes that all graphs are of the same size.

        for (Graph graph : graphs) {
            drawGraph(graphics2D, graph.getColor(), diagramMax, graph.getValues());
        }

        graphics2D.setClip(
                graphics2D.getClipBounds().x,
                graphics2D.getClipBounds().y + graphics2D.getClipBounds().height,
                graphics2D.getClipBounds().width,
                HEIGHT - graphics2D.getClipBounds().y - graphics2D.getClipBounds().height
        );
        drawLegend(graphics2D, List.of(graphs));

        graphics2D.drawImage(bufferedImage, null, null);
        graphics2D.dispose();

        return bufferedImage;
    }

    public void drawGraph(Graphics2D g, Color lineColor, int maxScaleValue, List<Integer> pointsY) {
        Stroke tmpStroke = g.getStroke();
        Color tmpColor = g.getColor();

        int clipHeight = g.getClipBounds().height;
        int clipWidth = g.getClipBounds().width;

        int sliceWidth = getGraphStepSizeX(g, pointsY.size());
        double yNormalizationFactor = clipHeight / maxScaleValue;   //the scaling factor to transform data points to the size of the draw area

        g.setColor(lineColor);
        g.setStroke(new BasicStroke(2));

        List<Point> points = new ArrayList<>(pointsY.size());
        int x = getGraphStartX(g);
        for (int i = 0; i < pointsY.size(); i++) {
            int y = g.getClipBounds().y + (int) (clipHeight - pointsY.get(i) * yNormalizationFactor);
            points.add(new Point(x,y));
            x = x + sliceWidth;
        }

        Point fromPoint = new Point(points.get(0));
        for(int i = 0; i < points.size(); i++) {
            Point toPoint = points.get(i);
            g.drawLine(fromPoint.x, fromPoint.y, toPoint.x, toPoint.y);
            fromPoint = toPoint;
        }

        g.setColor(tmpColor);
        g.setStroke(tmpStroke);
    }

    public void drawTitle (Graphics2D g, String title, Color color) throws IOException, FontFormatException {
        Color tmpColor = g.getColor();
        Stroke tmpStroke = g.getStroke();
        Font tmpFont = g.getFont();

        g.setColor(color);

        g.setFont(loadFontResource(FONT).deriveFont(g.getClipBounds().height * 0.06f));
        g.drawString(
                title,
                (g.getClipBounds().x + g.getClipBounds().width) / 2 - g.getFontMetrics().stringWidth(title) / 2,
                g.getClipBounds().y + g.getFont().getSize() + 5
        );


        g.setColor(tmpColor);
        g.setStroke(tmpStroke);
        g.setFont(tmpFont);
    }

    private void drawBackground(Graphics2D g, Color backgroundColor) {
        Color tmpColor = g.getColor();
        g.setColor(backgroundColor);
        g.fill(g.getClip());        //fill background
        g.setColor(tmpColor);
    }

    private void drawRaster(Graphics2D g, Color color, int segments, int maxDiagramValue, List<String> abscissaLabels) {

        if (segments < 1) {
            throw new IllegalArgumentException("segments must be > 0");
        }

        Color tmpColor = g.getColor();
        Stroke tmpStroke = g.getStroke();
        Font tmpFont = g.getFont();

        g.setColor(color);

        int segmentHeight = g.getClipBounds().height / segments;
        int lineValue = maxDiagramValue - (maxDiagramValue / segments);
        for (int i = 1; i < segments; i++) {
            g.drawLine(g.getClipBounds().x, g.getClipBounds().y + segmentHeight * i,
                    g.getClipBounds().x + g.getClipBounds().width, g.getClipBounds().y + segmentHeight * i);

            g.drawString(String.valueOf(lineValue), g.getClipBounds().x + 2, g.getClipBounds().y + segmentHeight * i + 12);
            lineValue -= maxDiagramValue / segments;
        }

        //draw labels
        int segmentWidth = g.getClipBounds().width / abscissaLabels.size();
        int x = getGraphStartX(g);
        int offsetX = getGraphStepSizeX(g, abscissaLabels.size());
        int offsetY = g.getFontMetrics().getHeight() + 3;
        for (int i = 0; i < abscissaLabels.size(); i++) {
            String label = abscissaLabels.get(i);
            g.drawString(label, x - g.getFontMetrics().stringWidth(label) / 2, g.getClipBounds().y + 10 /* + (i % 2 == 0 ? 0 : offsetY) */);
            x = x + offsetX;
        }

        g.setColor(tmpColor);
        g.setStroke(tmpStroke);
        g.setFont(tmpFont);
    }

    private void drawLegend(Graphics2D g, List<Graph> legendItems) throws IOException, FontFormatException {
        Color tmpColor = g.getColor();
        Font tmpFont = g.getFont();

        int itemGap = 20;
        int colorRectHeight = g.getClipBounds().height / 3;
        int colorRectWidth = colorRectHeight;

        g.setFont(loadFontResource(FONT).deriveFont((float) colorRectHeight * 1.1f));
        int offset = g.getClipBounds().x;
        for (var item : legendItems) {
            g.setColor(item.getColor());
            int stringHeight = g.getFontMetrics().getHeight();
            int stringWidth = g.getFontMetrics().stringWidth(item.getName());

            int drawBeginY = (int) (g.getClipBounds().y + g.getClipBounds().height / 2 - colorRectHeight / 2);

            g.fillRect(offset, drawBeginY, colorRectWidth, colorRectHeight);
            g.setColor(Color.BLACK);
            g.drawString(item.getName(), offset + colorRectWidth + itemGap / 3, drawBeginY + g.getFontMetrics().getHeight() / 1.5f);
            offset = offset + stringWidth + colorRectWidth + itemGap / 2 + itemGap;
        }

        g.setColor(tmpColor);
        g.setFont(tmpFont);
    }

    public File saveImage(String location, BufferedImage image) throws IOException {
        Path imageFile = Paths.get(location);
        ImageIO.write(image, "png", imageFile.toFile());
        return imageFile.toFile();
    }

    private Shape getScaledClip(Shape fullSizedClip, float scale) {
        Shape scaledClip = new Rectangle2D.Float(
                (float) (fullSizedClip.getBounds2D().getWidth() * scale),
                (float) (fullSizedClip.getBounds2D().getHeight() * scale),
                (float) (fullSizedClip.getBounds2D().getWidth() - (fullSizedClip.getBounds2D().getWidth() * scale) * 2),
                (float) (fullSizedClip.getBounds2D().getHeight() - fullSizedClip.getBounds2D().getHeight() * scale * 2)
        );

        return scaledClip;
    }

    private int getGraphStartX(Graphics2D g) {
        return (int) (g.getClipBounds().x + g.getClipBounds().width * 0.05f);
    }

    private Font loadFontResource(String fontLocation) throws IOException, FontFormatException {
        InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(fontLocation);
        return Font.createFont(Font.TRUETYPE_FONT, is);
    }

    private int getGraphStepSizeX(Graphics2D g, int numberOfDataPoints) {
        return g.getClipBounds().width / numberOfDataPoints;    //the step size in x direction per data point;
    }
}
