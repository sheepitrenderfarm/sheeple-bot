package bot.interactionresponders;

import bot.commands.Command;
import bot.settings.Settings;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ErrorResponder extends BaseInteractionResponder {

    public ErrorResponder(Settings settings) {
        super(settings);
    }

    @Override
    public void handle(SlashCommandInteractionEvent event, Command command) {
        MessageEmbed errorMsg = Settings.getInstance().createMessage("An error occurred while checking for requirements");
        Logger.getLogger(Settings.LOGGER_NAME).log(Level.SEVERE, () -> String.format("%s called %s in %s: %s", event.getMember(), event.getName(),
                event.getChannel().asTextChannel().getName(), errorMsg));
        event.replyEmbeds(errorMsg).queue();
    }
}
