package bot.interactionresponders;

import bot.commands.Command;
import bot.settings.Settings;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

public class IgnoreResponder extends BaseInteractionResponder {

    public IgnoreResponder(Settings settings) {
        super(settings);
    }

    @Override
    public void handle(SlashCommandInteractionEvent event, Command command) {
        return;
    }
}
