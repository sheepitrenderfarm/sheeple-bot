package bot.interactionresponders;

import bot.commands.Command;
import bot.settings.Settings;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.exceptions.ErrorHandler;

import java.util.concurrent.TimeUnit;

public class InvalidArgumentResponder extends BaseInteractionResponder {

    public InvalidArgumentResponder(Settings settings) {
        super(settings);
    }

    @Override
    public void handle(SlashCommandInteractionEvent event, Command command) {
        MessageEmbed errorMsg = Settings.getInstance().createMessage("Invalid argument");
        event.replyEmbeds(errorMsg).queue(
                m -> m.deleteOriginal().queueAfter(settings.getDeleteDelaySeconds(), TimeUnit.SECONDS,
                        null, new ErrorHandler())
        );
    }
}
