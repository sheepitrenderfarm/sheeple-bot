package bot.interactionresponders;

import bot.commands.Command;
import bot.errorhandlers.MessageDeleteErrorHandler;
import bot.settings.Settings;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

import java.util.concurrent.TimeUnit;

public class NoPermissionHandler extends BaseInteractionResponder {

    public NoPermissionHandler(Settings settings) {
        super(settings);
    }

    @Override
    public void handle(SlashCommandInteractionEvent event, Command command) {
        StringBuilder errorMsg = new StringBuilder(String.format("You need the %s permission to do that", command.getRequiredPermission()));
        MessageEmbed msg = Settings.getInstance().createMessage(errorMsg.toString());

        event.replyEmbeds(msg).queue(
                m -> m.deleteOriginal().queueAfter(Settings.getInstance().getDeleteDelaySeconds(), TimeUnit.SECONDS,
                        null, new MessageDeleteErrorHandler())
        );
    }
}
