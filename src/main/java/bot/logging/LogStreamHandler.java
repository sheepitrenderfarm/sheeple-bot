package bot.logging;

import lombok.Getter;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.logging.*;

public class LogStreamHandler extends StreamHandler {

    protected OutputStream out;
    @Getter private volatile boolean connectionBroke;

    public LogStreamHandler(OutputStream out, Formatter formatter) {
        super(out, formatter);
        this.out = out;
        this.connectionBroke = false;
//        BackgroundTaskHandler.get().addBackgroundTask(new LogStreamHandlerReconnectTask(this));
    }

    @Override
    public void setOutputStream(OutputStream out) {
        this.out = out;
        this.connectionBroke = false;
        super.setOutputStream(out);
    }

    @Override
    public void close() {
        flush();
        super.close();
    }

    @Override
    public void publish(LogRecord record) {
        if(!isLoggable(record) || connectionBroke)
            return;

        String log = getFormatter().format(record);
        try {
            out.write(log.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            connectionBroke = true;
            Logger.getLogger(getClass().getName()).log(Level.WARNING, e.getMessage());
        }
        flush();
    }
}
