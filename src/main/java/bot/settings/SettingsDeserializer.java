package bot.settings;

import bot.commands.Command;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.awt.*;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SettingsDeserializer extends JsonDeserializer<Settings> {

    @Override
    public Settings deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        Settings settings = new Settings();
        JsonNode node = p.getCodec().readTree(p);

        JsonNode tmp = node.get("startDate");
        if(tmp != null) {
            settings.setStartDate(LocalDate.parse(tmp.textValue()));
        }
        else {
            settings.setStartDate(LocalDate.now());
        }

        try {
            Optional.ofNullable(node.get("piHighscore")).ifPresent(r -> settings.setPiHighscore(r.asLong()));
        } catch (NullPointerException e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "NPE when trying to assign piHighscore!");
            settings.setPiHighscore(0);
        }

        try {
            Optional.ofNullable(node.get("resetCounter")).ifPresent(r -> settings.setResetCounter(r.asInt()));
        } catch (NullPointerException e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "NPE when trying to assign resetCounter!");
            settings.setResetCounter(0);
        }

        Optional.ofNullable(node.get("roleChannelID")).ifPresent(r -> settings.setRoleChannelID(r.textValue()));

        Optional.ofNullable(node.get("registrationChannelID")).ifPresent(r -> settings.setRegistrationChannelID(r.textValue()));

        Optional.ofNullable(node.get("mutedChannelID")).ifPresent(r -> settings.setMutedChannelID(r.textValue()));

        Optional.ofNullable(node.get("victimReportChannelID")).ifPresent(r -> settings.setVictimReportChannelID(r.textValue()));

        Optional.ofNullable(node.get("logChannelID")).ifPresent(r -> settings.setLogChannelID(r.textValue()));

        Optional.ofNullable(node.get("mutedRoleID")).ifPresent(r -> settings.setMutedRoleID(r.textValue()));

        Optional.ofNullable(node.get("memberRoleID")).ifPresent(r -> settings.setMemberRoleID(r.textValue()));

        Optional.ofNullable(node.get("blackListURL")).ifPresent(r -> settings.setBlackListURL(r.textValue()));

        Optional.ofNullable(node.get("serverBlackListURL")).ifPresent(r -> settings.setServerBlackListURL(r.textValue()));

        Optional.ofNullable(node.get("alertRoleID")).ifPresent(r -> settings.setAlertRoleID(r.asLong()));

        Optional.ofNullable(node.get("modRoleID")).ifPresent(r -> settings.setModeratorRoleID(r.asLong()));

        Optional.ofNullable(node.get("spamVoteThreshold")).ifPresent(r -> settings.setSpamVoteThreshold(r.asInt()));

        Optional.ofNullable(node.get("spamVoteDuration")).ifPresent(r -> settings.setSpamVoteDuration(r.asLong()));

        Optional.ofNullable(node.get("callScamVote")).ifPresent(r -> settings.setCallScamVote(r.asBoolean()));

        Optional.ofNullable(node.get("deleteScam")).ifPresent(r -> settings.setDeleteScam(r.asBoolean()));

        Optional.ofNullable(node.get("muteScammers")).ifPresent(r -> settings.setMuteScammers(r.asBoolean()));

        Optional.ofNullable(node.get("scamDeletionMessage")).ifPresent(r -> settings.setScamDeletionMessage(r.textValue()));

        Optional.ofNullable(node.get("scamAlertMessage")).ifPresent(r -> settings.setScamAlertMessage(r.textValue()));

        Optional<JsonNode> tokenResult = Optional.ofNullable(node.findValue("discordToken"));
        if (!tokenResult.isPresent())
            return null;
        tokenResult.ifPresent(r -> settings.setDiscordToken(r.textValue()));

        try {
            Optional.ofNullable(node.get("guildID")).ifPresent(r -> settings.setGuildID(r.textValue()));
        } catch (NullPointerException e) {
//            Logger.getLogger(getClass().getName().log)
        }

        Optional.ofNullable(node.get("sheepitAdminRoleIDs")).ifPresent(r -> {
            settings.getSuperRoleIDs().clear();
            r.forEach(id -> settings.getSuperRoleIDs().add(id.textValue()));
        });

        Optional.ofNullable(node.get("assignableRoleIDs")).ifPresent(r -> {
            settings.getAssignableRoles().clear();
            r.forEach(id -> settings.getAssignableRoles().add(id.textValue()));
        });

        Optional.ofNullable(node.get("statsEndpoint")).ifPresent(r -> settings.setStatsEndpoint(r.textValue()));

        Optional.ofNullable(node.get("kickReason")).ifPresent(r -> settings.setKickReason(r.textValue()));

        Optional.ofNullable(node.get("registrationGracePeriod")).ifPresent(r -> settings.setKickUnregisteredMembersAfter_ms(r.asLong()));

        try {
            JsonNode msgConfig;
            msgConfig = node.get("messageConfig");
            if (msgConfig != null) {
                Optional.ofNullable(msgConfig.get("messageTitle")).ifPresent(r -> {
                    settings.setMessageTitle(r.textValue());
                    settings.getMessageBuilder().setTitle(r.textValue());
                });

                Optional.ofNullable(msgConfig.get("messageAuthor")).ifPresent(r -> settings.setMessageAuthor(r.textValue()));
                Optional.ofNullable(msgConfig.get("messageURL")).ifPresent(r -> settings.setMessageURL(r.textValue()));
                Optional.ofNullable(msgConfig.get("messageIconURL")).ifPresent(r -> settings.setMessageIconURL(r.textValue()));
                settings.getMessageBuilder().setAuthor(settings.getMessageAuthor(),
                        settings.getMessageURL(),
                        settings.getMessageIconURL());

                Optional.ofNullable(msgConfig.get("messageThumbnailURL")).ifPresent( r -> {
                    settings.setMessageThumbnailURL(r.textValue());
                    try {
                        settings.getMessageBuilder().setThumbnail(r.textValue());
                    }
                    catch (IllegalArgumentException e) {
                        Logger.getLogger(Settings.LOGGER_NAME).log(Level.SEVERE, () -> String.format("Cannot set messageThumbnailURL: %s - %s",
                                e.getCause(), e.getMessage()));
                    }
                });

                Optional.ofNullable(msgConfig.get("messageColor")).ifPresent( r -> {
                    settings.setMessageColor(r.textValue());
                    settings.getMessageBuilder().setColor(Color.decode(r.textValue()));
                });

            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        tmp = node.get("commands");
        if(tmp != null) {
            tmp.elements().forEachRemaining(commandNode -> {
                Command cmd;
                try {
                    cmd = new ObjectMapper().readValue(commandNode.toString(), Command.class);
                    if(cmd != null) {
                        settings.getCommands().put(cmd.getName(), cmd);
                    }
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            });
        }
        return settings;
    }
}
